class Car < ApplicationRecord
  belongs_to :manufacturer
  has_many :sales
  has_many :rentals
end
