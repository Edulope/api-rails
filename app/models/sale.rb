class Sale < ApplicationRecord
  belongs_to :car
  before_create() do
    if car.storage > 0
      car.storage -= 1
    else
      errors.add(:category, "out of storage")
    end
  end

  def storageCheck 
    if car.storage != 0
        return "#{car.storage} in storage"
    end
  end
end
