class CarSerializer < ActiveModel::Serializer
  attributes :id, :manufacturer, :name, :storage, :status, :daily, :monthly, :yearly, :price
end
