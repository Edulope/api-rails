class RentalSerializer < ActiveModel::Serializer
  attributes :id, :begin, :end, :rental_price
end
