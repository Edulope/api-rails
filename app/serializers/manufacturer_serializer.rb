class ManufacturerSerializer < ActiveModel::Serializer
  attributes :id, :name, :cars
end
