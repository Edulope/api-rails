Rails.application.routes.draw do
  devise_for :users, defaults: {format: :json}
  resources :sales
  resources :rentals
  resources :cars
  resources :manufacturers


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
