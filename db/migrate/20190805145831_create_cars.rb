class CreateCars < ActiveRecord::Migration[5.2]
  def change
    create_table :cars do |t|
      t.string :name
      t.integer :storage
      t.integer :status
      t.integer :daily
      t.integer :monthly
      t.integer :yearly
      t.integer :price
      t.references :manufacturer, foreign_key: true

      t.timestamps
    end
  end
end
