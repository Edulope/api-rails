class CreateRentals < ActiveRecord::Migration[5.2]
  def change
    create_table :rentals do |t|
      t.integer :begin
      t.integer :end
      t.integer :rental_price

      t.timestamps
    end
  end
end
