class AddInfoToSale < ActiveRecord::Migration[5.2]
  def change
    add_reference :sales, :car, foreign_key: true
  end
end
