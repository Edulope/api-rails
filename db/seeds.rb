# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Manufacturer.create(name: "fiat");
Car.create(name: "carro1", manufacturer_id: 1, price: 10000, status:1, storage:200)
Sale.create(car_id: 1);
